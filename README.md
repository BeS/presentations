Here you can find most of my presentations about Free Software, ownCloud and
similar stuff.

Why? Let me quote Linus Torvalds: "Real men don't make backups. They upload it
via ftp and let the world mirror it."
